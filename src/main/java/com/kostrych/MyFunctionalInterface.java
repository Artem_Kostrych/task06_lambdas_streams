package com.kostrych;

@FunctionalInterface
public interface MyFunctionalInterface {
    int method(int a, int b, int c);
}
