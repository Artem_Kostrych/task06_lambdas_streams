package com.kostrych;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private static Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Command> methodsMenu;
    private HomeWork homeWork= new HomeWork();

    public Menu() {
        menu = new LinkedHashMap<String, String>();
        methodsMenu = new LinkedHashMap<String, Command>();
        menu.put("1", "1-Show task 6.1");
        menu.put("2", "2-Show task 6.3");
        menu.put("3", "3-Show task 6.4");
        menu.put("4", "4-Show information about authors.");
        menu.put("Q", "Q-Exit");

        Command command = new Command() {
            @Override
            public void call() {
                aboutAuthor();
            }
        };

        methodsMenu.put("1", ()->{firstTask();});
        methodsMenu.put("2", this::thirdTask);
        methodsMenu.put("3", new Command() {
            @Override
            public void call() {
                fourthTask();
            }
        });
        methodsMenu.put("4", command);
    }

    public void thirdTask(){
        homeWork.thirdTask();
    }
    public void firstTask(){
        homeWork.firstTask();
    }

    public void aboutAuthor(){
        System.out.println("Project developer - Artem Kostrych.");
    }

    public void fourthTask(){
        homeWork.fourthTask();
    }

    void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }


    public void show() {
        String keyMenu;
        do {
            System.out.print((char)27 + "[31m");
            outputMenu();
            System.out.print((char)27 + "[34m");
            System.out.println("\nPlease, make your choice: ");
            System.out.print((char)27 + "[33m");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).call();
            } catch (Exception e) {
                System.out.println("Exceptions");
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
