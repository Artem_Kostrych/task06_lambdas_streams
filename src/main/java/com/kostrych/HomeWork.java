package com.kostrych;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class HomeWork {
    public void firstTask() {
        MyFunctionalInterface lambdaMax = (a, b, c) -> a > b ? a > c ? a : c : b;
        System.out.println("MAX element: " + lambdaMax.method(12, 8, 15));
        MyFunctionalInterface lambdaAvg = (a, b, c) -> (a + b + c) / 3;
        System.out.println("AVG : " + lambdaAvg.method(1, 2, 3));
    }

    public void thirdTask() {
        List<Integer> integers = returnRandomInt();
        System.out.print("\nList of elements: ");
        integers.stream().forEach(v -> System.out.print(v + " "));
        // integers=returnRandomInt();

        System.out.println("\n\nSum of element: "
                + integers.stream().mapToDouble(s -> (double) s).sum());

        System.out.println("Average of list: "
                + integers.stream().mapToDouble(s -> (double) s).average());

        System.out.println("MAX: "
                + integers.stream().max(Integer::compareTo).get());

        System.out.println("MIN: "
                + integers.stream().min(Integer::compareTo).get());

        long i = integers.stream().
                filter(s -> s > integers.stream().mapToDouble(v -> (double) v).average().getAsDouble())
                .count();
        System.out.println("Amount of element which is more than average: " + i);
    }

    private List<Integer> returnRandomInt() {
        List<Integer> integers = new ArrayList<>();
        integers.add((int) (Math.random() * 10));
        for (int i = 0; i < Math.random() * 15; i++) {
            integers.add((int) (Math.random() * 100));
        }
        return integers;
    }

    public void fourthTask() {
        System.out.println("\nInput text: ");
        Scanner in = new Scanner(System.in);
        List<String> strings = new LinkedList<>();
        int i = 0;
        do {
            int firstIndex = 0;
            int secondIndex = 0;
            String s = new String(in.nextLine());
            if (s.length() == 0)
                break;
            while (secondIndex >= 0 && s.length() > 0) {
                secondIndex = s.indexOf(" ", firstIndex + 1);
                if (secondIndex == -1)
                    break;
                ;
                strings.add(s.substring(firstIndex, secondIndex));
                firstIndex = secondIndex + 1;
                i++;
            }
        } while (strings.get(i - 1).length() != 0);
        strings.stream().forEach(v -> System.out.println(v));
        List<String> sortedList = strings.stream().distinct().collect(Collectors.toList());
        System.out.println("\n\nWithout repeat:");
        sortedList.stream().forEach(v -> System.out.println(v));
        sortedList = sortedList.stream().sorted().collect(Collectors.toList());
        System.out.println("\nSorted unique words:");
        sortedList.stream().forEach(v -> System.out.println(v));
    }
}
